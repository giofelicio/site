<?php

use Twig\Loader\FilesystemLoader;

require_once __DIR__.'/vendor/autoload.php';
 
$loader = new FilesystemLoader(__DIR__.'/view');
$twig = new \Twig\Environment($loader);