{% extends "layouts/default.php" %}

{% block head %}

    <link rel="stylesheet" href="{{url}}/public/css/select2.min.css">
    <link rel="stylesheet" href="{{url}}/public/css/dataTables.bootstrap4.min.css">
    <script src="{{url}}/public/js/jquery.min.js"></script>

{% endblock %} 
{% block body %}
    <div class="container">
        {% if success %}
            <div class="alert alert-success">
                <li>{{success}}</li>
            </div>
        {%endif%}
        {% if errors %}
            {% for error in errors %}
                <div class="alert alert-danger">
                    <li>{{error}}</li>
                </div>
            {% endfor %}
        {%endif%}
        <div class="row">
            <div class="col mt-5">
                <button class="btn btn-info float-right" data-toggle="modal" data-target="#addQuiz"><i class="fa fa-plus mr-2"></i>ADD QUESTION</button>
            </div>
        </div>
        <hr>
        {% if questions %}
            {% for i,qn in questions %}
                <div class="row">
                    <div class="col-12">
                        <div class="card bg-light mb-3" style="max-width: 100%;">
                            <div class="card-header">{{i+1}}) {{qn.question}}</div>
                            <div class="card-body">
                                <div class="card-text">
                                    {% if qn.type == 1 %}
                                        {% if qn.answer != null %}
                                            <textarea onkeyup="active('{{i}}')" name="answer{{i}}" id="answer{{i}}" cols="30" rows="10" class="form-control">{{qn.answer}}</textarea>
                                        {% else %}
                                            <textarea onkeyup="active('{{i}}')" name="answer{{i}}" id="answer{{i}}" cols="30" rows="10" class="form-control"></textarea>
                                        {% endif %}
                                    {% elseif qn.type == 2 %}
                                        {% for j,model in qn.model %}
                                            <div class="custom-control custom-radio">
                                                {% if qn.answer == qn.value[j] %}
                                                    <input checked type="radio" class="custom-control-input" id="answer{{i}}{{j}}" name="answer{{i}}" value="{{qn.value[j]}}">
                                                {% else %}
                                                    <input onchange="active('{{i}}')" type="radio" class="custom-control-input" id="answer{{i}}{{j}}" name="answer{{i}}" value="{{qn.value[j]}}">
                                                {% endif %}
                                                <label class="custom-control-label" for="answer{{i}}{{j}}">{{model}}</label>
                                            </div>
                                        {% endfor %}
                                    {% endif %}
                                    <br>
                                    <div class="float-right row align-items-center mr-1">
                                        <div id="divButtonSave{{i+1}}">
                                            {% if qn.answer != null %} 
                                                <i class="fa fa-check text-success mr-2"></i>
                                            {% else %}
                                                <i class="fa fa-ban text-danger mr-2"></i>
                                            {% endif %}
                                        </div>
                                        <button disabled type="button" data-id="{{qn.id}}" id="button{{i}}" data-answer="answer{{i}}" class="btn btn-secondary saveQuestion">Save Answer</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            {% endfor %}
        {% endif %}
    </div>
    <div class="modal fade" id="addQuiz" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"> ADD QUESTION </h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>X</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="/qnr/qn" id="createQuestion">
                        <input hidden name="reference" value="{{reference}}">
                        <div class="form-group">
                            <label>Question:</label>
                            <textarea name="question" id="question" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Type Question:</label>
                            <select name="type" id="type" class="form-control">
                                <option selected disabled>Select the type question</option>
                                <option value="1">Open Question</option>
                                <option value="2">Closed Question</option>
                            </select>
                        </div>
                        <div id="divModel" class="form-group">
                            <div id="type1" hidden>
                                <label for="model" class="col-form-label">Question Model</label>
                                <select disabled name="model" id="model" class="form-control">
                                    <option selected disabled>Select the Model</option>
                                    {% for model in modelsRa %}
                                        <option value="{{model.id}}">{{model.model}}</option>
                                    {% endfor %}
                                    <option value="0">Another Model</option>
                                </select>
                            </div>
                        </div>
                        <div id="divModelAnother" class="form-group" hidden>
                            <label for="anotherModel" class="col-form-label">Question Model</label>
                            <div class="input-group">
                                <select multiple name="anotherModel[]" id="anotherModel" style="width: 100%" disabled></select>
                            </div>
                            <label for="modelValue" class="col-form-label">Model Value</label>
                            <div class="input-group">
                                <select multiple name="value[]" id="modelValue" style="width: 100%" disabled></select>
                            </div>
                            <br>
                            <div class="custom-control custom-checkbox">
                                <input disabled name="checkToSave" type="checkbox" class="custom-control-input" id="checkToSave" onchange="this.value = this.checked ? 1 : 0;">
                                <label class="custom-control-label" for="checkToSave">Save model</label>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">ADD</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
{% endblock %} 
{% block js %}
    <script src="{{url}}/public/js/bootstrap.js"></script>
    <script src="{{url}}/public/js/popper.min.js"></script>
    <script src="{{url}}/public/js/jquery.datatable.min.js"></script>
    <script src="{{url}}/public/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{url}}/public/js/select2.min.js"></script>
    <script>
        $('#anotherModel').select2({
            tags: true,
            tokenSeparators: ['#'],
            placeholder: "Add model here",
            selectOnClose: true,
            closeOnSelect: false
        });
        $('#modelValue').select2({
            tags: true,
            tokenSeparators: ['#'],
            placeholder: "Add values here",
            selectOnClose: true,
            closeOnSelect: false
        });


        $(".alert").fadeTo(3200, 800).slideUp(1000, function(){
			$(".alert").slideUp(500);
		});
        $('#type').on('change', function(){
            let value = $('#type').val();
            if(value == 1){
                $('#type1').prop('hidden', true);$("#model").prop('disabled', true);
                $('#divModelAnother').prop('hidden', true);$('#anotherModel').prop('disabled', true);
                $('#checkToSave').prop('disabled', true);$('#modelValue').prop('disabled', true);
            }
            else if(value == 2){
                $('#type1').prop('hidden', false);$("#model").prop('disabled', false);
                $('#divModelAnother').prop('hidden', true);$('#anotherModel').prop('disabled', true);
                $('#checkToSave').prop('disabled', true);$('#modelValue').prop('disabled', true);
            }
        });
        $('#model').on('change', function(){
            let value = $(this).val();
            if(value == 0){
                $('#divModelAnother').prop('hidden', false);$('#anotherModel').prop('disabled', false);
                $('#checkToSave').prop('disabled', false);$('#modelValue').prop('disabled', false);
            }
            else{
                $('#divModelAnother').prop('hidden', true);$('#anotherModel').prop('disabled', true);
                $('#checkToSave').prop('disabled', true);$('#modelValue').prop('disabled', true);
            }
        });
        $('.saveQuestion').click(function(){
            var idQuestion = $( this ).attr('data-id');
            var answer;
            if($('input[name='+$(this).attr('data-answer')+']:checked').val() == undefined){
                answer = $('#'+$(this).attr('data-answer')+'').val(); 
            }
            else{
                answer = $('input[name='+$(this).attr('data-answer')+']:checked').val(); 
            }
            if(typeof answer != undefined){
                $.ajax({
                    type: "POST",
                    url: "/savequestion",
                    data: {idQuestion: idQuestion, answer:answer},
                    success: function(data){
                        if(data == 1){
                            $('#divButtonSave'+idQuestion+'').html(' ');
                            $('#divButtonSave'+idQuestion+'').html('<i class="fa fa-check text-success mr-2"></i>');
                        }
                        else{
                            $('#divButtonSave'+idQuestion+'').html(' ');
                            $('#divButtonSave'+idQuestion+'').html('<i class="fa fa-ban text-danger mr-2"></i>');
                        }
                    }
                });
                return false;
            }
            else{

            }
            /**/
        });
        function active(id){
            $('#button'+id+'').prop('disabled', false)
            $('#button'+id+'').removeClass('btn-secondary');
            $('#button'+id+'').addClass('btn-success');
        }
        
    </script>

{% endblock %} 