{% extends "layouts/default.php" %}

{% block head %}

    <link rel="stylesheet" href="{{url}}/public/css/dataTables.bootstrap4.min.css">
    <script src="{{url}}/public/js/jquery.min.js"></script>
    

{% endblock %} 
{% block body %}

    <div class="container">
        <div class="row">
            <div class="col mt-5">
                <button class="btn btn-info float-right" data-toggle="modal" data-target="#addUsers"><i class="fa fa-plus mr-2"></i>ADD USER</button>
                <div class="table-responsive mt-5">
                    <table id="users" class="table table-striped table-bordered users" style="width: 100%;">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Broadcast On</th>
                            <th>Speaker</th>
                            <th>Added In</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addUsers" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"> ADD USER </h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>X</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="/users" id="createUser">
                        <div class="form-group">
                            <label>Name:</label>
                            <input type="text" class="form-control" name="name">
                        </div>
                        <div class="form-group">
                            <label>Username:</label>
                            <input type="text" class="form-control" name="username">
                        </div>
                        <div class="form-group">
                            <label>E-mail:</label>
                            <input type="email" class="form-control" name="email">
                        </div>
                        <div class="form-group">
                            <label>E-mail:</label>
                            <input type="email" class="form-control" name="email">
                        </div>
                        <div class="form-group">
                            <label>Role:</label>
                            <select name="role" id="role" class="form-control">
                                <option selected disabled>Select the Role</option>
                                <option value="1">User</option>
                                <option value="2">Admin</option>
                            </select>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">ADD</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
{% endblock %} 
{% block js %}
    <script src="{{url}}/public/js/bootstrap.js"></script>
    <script src="{{url}}/public/js/popper.min.js"></script>
    <script src="{{url}}/public/js/jquery.datatable.min.js"></script>
    <script src="{{url}}/public/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{url}}/public/js/select2.min.js"></script>
    <script>
        $('#users').DataTable({
            "scrollY": "500px",
            "scrollCollapse": true
        });
    </script>

{% endblock %} 