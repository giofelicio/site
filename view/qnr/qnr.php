{% extends "layouts/default.php" %}

{% block head %}

    <link rel="stylesheet" href="{{url}}/public/css/dataTables.bootstrap4.min.css">
    <script src="{{url}}/public/js/jquery.min.js"></script> 

{% endblock %} 
{% block body %}

    <div class="container">
        {% if success %}
            <div class="alert alert-success">
                <li>{{success}}</li>
            </div>
        {%endif%}
        {% if errors %}
            {% for errors in error %}
                <div class="alert alert-danger">
                    <li>{{error}}</li>
                </div>
            {% endfor %}
        {%endif%}
        <div class="row">
            <div class="col mt-5">
                <button class="btn btn-info float-right" data-toggle="modal" data-target="#addQuiz"><i class="fa fa-plus mr-2"></i>ADD QUIZ</button>
                <div class="table-responsive mt-5">
                    <table id="qnrs" class="table table-bordered qnrs text-center" style="width: 100%;">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Create By</th>
                            <th>Added In</th>
                            <th>Status</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addQuiz" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"> ADD </h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>X</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="" id="createQnr">
                        <div class="form-group">
                            <label>Name</label>
                            <input class="form-control" id="name" type="text" name="name" placeholder="Ex: Evaluation" autofocus>
                        </div>
                        <div class="form-group">
                            <label>Sector</label>
                            <select name="sector" id="sector" class="form-control">
                                <option selected disabled>Select the Sector</option>
                                {% for sector in sectors %}
                                    <option value="{{sector.id}}">{{sector.name}}</option>
                                {% endfor %}
                            </select>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">ADD</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
{% endblock %} 
{% block js %}
    <script src="{{url}}/public/js/bootstrap.js"></script>
    <script src="{{url}}/public/js/popper.min.js"></script>
    <script src="{{url}}/public/js/jquery.datatable.min.js"></script>
    <script src="{{url}}/public/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(".alert").fadeTo(3200, 800).slideUp(1000, function(){
			$(".alert").slideUp(500);
		});
        var table = $('#qnrs').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "/getdataqnr/{{reference}}",
            "columns": [
                { "data": "name" },
                { "data": "create_by" },
                { "data": "created_at" },
                { "data": "status" },
                { "data": "action" }],
            "scrollY": "500px",
            "scrollCollapse": true
        });
        $('#createQnr').submit(function(){
            var dados = $( this ).serialize();
            $.ajax({
                type: "POST",
                url: "/qnr",
                data: dados,
                success: function(data){
                    if(data == 1){
                        $('#addQuiz').modal('hide');
                        $('#qnrs').DataTable().ajax.reload();
                    }
                }
            });
            return false;
        });
    </script>

{% endblock %} 