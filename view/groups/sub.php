{% extends "layouts/default.php" %}

{% block head %}

    <link rel="stylesheet" href="{{url}}/public/css/dataTables.bootstrap4.min.css">
    <script src="{{url}}/public/js/jquery.min.js"></script> 

{% endblock %} 
{% block body %}

    <div class="container">
        {% if success %}
            <div class="alert alert-success">
                <li>{{success}}</li>
            </div>
        {%endif%}
        {% if errors %}
            {% for errors in error %}
                <div class="alert alert-danger">
                    <li>{{error}}</li>
                </div>
            {% endfor %}
        {%endif%}
        <div class="row">
            <div class="col mt-5">
                <button class="btn btn-info float-right" data-toggle="modal" data-target="#addSubGroup"><i class="fa fa-plus mr-2"></i>ADD SUB GROUP</button>
                <div class="table-responsive mt-5">
                    <table id="subgroups" class="table table-bordered subgroups text-center" style="width: 100%;">
                        <thead>
                        <tr>
                            <th>N°</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Added In</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addSubGroup" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"> ADD </h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>X</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="" id="createSubGroup">
                        <input value="{{reference}}" name="reference" hidden/>
                        <div class="form-group">
                            <label>SubGroup Name</label>
                            <input class="form-control" id="name" type="text" name="name" placeholder="Ex: Realização da compra" autofocus>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">ADD</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
{% endblock %} 
{% block js %}
    <script src="{{url}}/public/js/bootstrap.js"></script>
    <script src="{{url}}/public/js/popper.min.js"></script>
    <script src="{{url}}/public/js/jquery.datatable.min.js"></script>
    <script src="{{url}}/public/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $('#subgroups').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "/groups/sub/getdata/{{reference}}",
            "columns": [
                { "data": "number" },
                {"data": "name"},
                { "data": "status" },
                { "data": "created_at"},
                { "data": "action" }],
            "scrollY": "1000px",
            "scrollCollapse": true
        });
        $('#anotherModel').select2({
            tags: true,
            tokenSeparators: ['#'],
            placeholder: "Add model here",
            selectOnClose: true,
            closeOnSelect: false
        });
        $('#modelValue').select2({
            tags: true,
            tokenSeparators: ['#'],
            placeholder: "Add values here",
            selectOnClose: true,
            closeOnSelect: false
        });

        $(".alert").fadeTo(3200, 800).slideUp(1000, function(){
			$(".alert").slideUp(500);
		});
        $('#type').on('change', function(){
            let value = $('#type').val();
            if(value == 1){
                $('#type1').prop('hidden', true);$("#model").prop('disabled', true);
                $('#divModelAnother').prop('hidden', true);$('#anotherModel').prop('disabled', true);
                $('#checkToSave').prop('disabled', true);$('#modelValue').prop('disabled', true);
            }
            else if(value == 2){
                $('#type1').prop('hidden', false);$("#model").prop('disabled', false);
                $('#divModelAnother').prop('hidden', true);$('#anotherModel').prop('disabled', true);
                $('#checkToSave').prop('disabled', true);$('#modelValue').prop('disabled', true);
            }
        });
        $('#model').on('change', function(){
            let value = $(this).val();
            if(value == 0){
                $('#divModelAnother').prop('hidden', false);$('#anotherModel').prop('disabled', false);
                $('#checkToSave').prop('disabled', false);$('#modelValue').prop('disabled', false);
            }
            else{
                $('#divModelAnother').prop('hidden', true);$('#anotherModel').prop('disabled', true);
                $('#checkToSave').prop('disabled', true);$('#modelValue').prop('disabled', true);
            }
        });
        $('#createSubGroup').submit(function(){
            var dados = $( this ).serialize();
            $.ajax({
                type: "POST",
                url: "/groups/sub",
                data: dados,
                success: function(data){
                    if(data == 1){
                        $('#addSubGroup').modal('hide');
                        $('#subgroups').DataTable().ajax.reload();
                    }
                }
            });
            return false;
        });
    </script>

{% endblock %} 