<?php

namespace Middlewares;

use Exception;
use PDO;

class RedirectAuthMiddleware{
    private $PDO;

    public function __construct(){
      $this->PDO = new PDO(getenv('DB_CONNECTION').':host='.getenv('DB_HOST').';dbname='.getenv('DB_NAME').'', getenv('DB_USER'), getenv('DB_PASSWORD') ,array(PDO::ATTR_PERSISTENT => true));
        session_start();
    }
  public function handle(){
    if (isset($_SESSION["username"]) && isset($_SESSION["token"])){
      $sql = "SELECT * FROM sbr_users WHERE username = :username AND token = :token";
      $find = $this->PDO->prepare($sql);
      $find->bindValue(':username', $_SESSION["username"]);
      $find->bindValue(':token', $_SESSION["token"]);
      try{
        $result = $find->fetch(PDO::FETCH_OBJ);
        if($result == null){
            return true;
        }
      }
      catch(Exception $e){
        return true;
      }
      header('location: /');
    }
    else{
        return true;
    }
   
  }
}