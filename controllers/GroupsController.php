<?php
namespace Controllers;

use Exception;
use PDO;
use Twig\Loader\FilesystemLoader;

class GroupsController{

    private $twig;
    private $PDO;

    public function __construct(){
        $this->twig = new \Twig\Environment(new FilesystemLoader(dirname(__DIR__).'/view'));
        $this->PDO = new PDO(getenv('DB_CONNECTION').':host='.getenv('DB_HOST').';dbname='.getenv('DB_NAME').'', getenv('DB_USER'), getenv('DB_PASSWORD') ,array(PDO::ATTR_PERSISTENT => true));
    }
    public function index(){
        $errors = array();
        try {
            array_push($errors, $_COOKIE['error_msg'] ?? null);
        } catch (\Throwable $th) {
            //
        }
        $success = $_COOKIE['success_msg'] ?? null;
        setcookie("error_msg", "", time()-3600);
        setcookie("success_msg", "", time()-3600);
        $getGroups = $this->PDO->prepare('SELECT g.id, g.name, g.status, g.created_at FROM sbr_group g');
        try {
            $getGroups->execute();
            $groups = $getGroups->fetchAll(PDO::FETCH_OBJ);
        } catch (\Throwable $th) {
            array_push($errors, 'Error when listing groups');
        }
        foreach($groups as $group){
            $group->created_at = date('d-m-Y H:i:s',strtotime($group->created_at));
        }
        return $this->twig->render('/groups/groups.php', ['url'=> 'http://'.$_SERVER['HTTP_HOST'], 'username' => $_SESSION['username'], 'groups' => $groups,'errors' => $errors, 'success' => $success]);
    }
    public function create(){
        $name = isset($_POST["name"]) ? trim($_POST["name"]) : FALSE;
        $insert = $this->PDO->prepare('INSERT INTO sbr_group(name) VALUES(:name)');
        $insert->bindValue(':name', $name);
        try {
            $result = $insert->execute();
            if($result){
                setcookie("success_msg", 'Successfully Added' , mktime()+(60*1), '/');
                header('location: /groups');
            }
        } catch (Exception $e) {
            setcookie("error_msg", 'Error Adding' , mktime()+(60*1), '/');
            header('location: /groups');
            exit;
        }
        
    }
    public function questions($id){
        $errors = array();
        try {
            array_push($errors, $_COOKIE['error_msg'] ?? null);
        } catch (\Throwable $th) {
            //
        }
        $success = $_COOKIE['success_msg'] ?? null;
        setcookie("error_msg", "", time()-3600);
        setcookie("success_msg", "", time()-3600);
        return $this->twig->render('/groups/questions.php', ['url'=> 'http://'.$_SERVER['HTTP_HOST'], 'username' => $_SESSION['username'], 'reference' => $id,'errors' => $errors, 'success' => $success]);
    }
    public function subgroup($id){
        $errors = array();
        try {
            array_push($errors, $_COOKIE['error_msg'] ?? null);
        } catch (\Throwable $th) {
            //
        }
        $success = $_COOKIE['success_msg'] ?? null;
        setcookie("error_msg", "", time()-3600);
        setcookie("success_msg", "", time()-3600);
        return $this->twig->render('/groups/sub.php', ['url'=> 'https://'.$_SERVER['HTTP_HOST'], 'username' => $_SESSION['username'], 'reference' => $id,'errors' => $errors, 'success' => $success]);
    }
    public function createsub(){
        if(!isset($_POST['name']) || !isset($_POST['reference'])){
            return 0;
        }
        elseif($_POST['name'] == '' || !is_numeric($_POST['reference']) || $_POST['reference'] == ''){
            return 0;
        }
        if($this->findSubGroup($_POST['name']) == true){
            return 0;
        }
        $insert = $this->PDO->prepare('INSERT INTO sbr_group_sub(id_sbr_group, name) VALUES(:id_sub_group,:name)');
        try {
            $result = $insert->execute([':id_sub_group' => $_POST['reference'], ':name' => $_POST['name']]);
            if($result){
                return 1;
            }
            else{
                return 0;
            }
        } catch (Exception $e) {
            return 0;
        }
    }
    private function findSubGroup($name){
        $select = $this->PDO->prepare('SELECT * FROM site.sbr_group_sub WHERE name = :name');
        try {
            $select->execute([':name' => $name]);
            $subgroup = $select->fetch(PDO::FETCH_OBJ);
            if($subgroup){
                return true;
            }
            else{
                return false;
            }
        } catch (\Throwable $th) {
            return true;
        }
        return false;
    }

}
