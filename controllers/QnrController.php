<?php
namespace Controllers;

use Exception;
use PDO;
use Twig\Loader\FilesystemLoader;

class QnrController{

    private $twig;
    private $PDO;

    public function __construct(){
        $this->twig = new \Twig\Environment(new FilesystemLoader(dirname(__DIR__).'/view'));
        $this->PDO = new PDO(getenv('DB_CONNECTION').':host='.getenv('DB_HOST').';dbname='.getenv('DB_NAME').'', getenv('DB_USER'), getenv('DB_PASSWORD') ,array(PDO::ATTR_PERSISTENT => true));
    }
    public function index(){
        $errors = array();
        try {
            array_push($errors, $_COOKIE['error_msg'] ?? null);
        } catch (\Throwable $th) {
            //
        }
        $success = $_COOKIE['success_msg'] ?? null;
        setcookie("error_msg", "", time()-3600);
        setcookie("success_msg", "", time()-3600);
        $getSector = $this->PDO->prepare('SELECT s.id, s.name, s.created_at FROM sbr_sec s');
        try {
            $getSector->execute();
            $sectors = $getSector->fetchAll(PDO::FETCH_OBJ);
        } catch (\Throwable $th) {
            array_push($errors, 'Error when listing sectors');
        }
        foreach($sectors as $sector){
            $sector->created_at = date('d-m-Y H:i:s',strtotime($sector->created_at));
        }
        return $this->twig->render('/qnr/qnr.php', ['url'=> 'http://'.$_SERVER['HTTP_HOST'], 
                                                    'username' => $_SESSION['username'],
                                                    'reference' => $_SESSION['id'],
                                                    'sectors' => $sectors,
                                                    'errors' => $errors, 
                                                    'success' => $success]);
    }
    public function create(){
        $nameSec = isset($_POST["name"]) ? trim($_POST["name"]) : FALSE;
        $sector = isset($_POST["sector"]) ? trim($_POST["sector"]) : FALSE;
        $insert = $this->PDO->prepare('INSERT INTO site.sbr_qnr(`id_sbr_sec`, create_by, name) VALUES(:id , :idU,:name)');
        $insert->bindValue(':id', $sector);
        $insert->bindValue(':idU', $_SESSION["id"]);
        $insert->bindValue(':name', $nameSec);
        try {
            $result = $insert->execute();
            if($result){
                echo 1;
            }
            else{
                echo 0;
            }
        } catch (Exception $e) {
           echo 0;
        }
    }
    public function qnrupdate($id){
        if(!is_numeric($_POST['idQuestion'])){
            echo 0;
        }
        $updateQn = $this->PDO->prepare('UPDATE sbr_qnr SET status = :status WHERE id = :id');
        try {
            $result = $updateQn->execute([':status' => 2, ':id' => $id]);
            if($result){
                header('Location: /questionnarie/'.$id);
                exit;
            }
            else{
                setcookie("error_msg", 'Error when redirecting' , mktime()+(60*1), '/');
                header('Location: ' . $_SERVER['HTTP_REFERER']);
                exit;
            }
        } catch (Exception $e) {
            setcookie("error_msg", 'Error when redirecting' , mktime()+(60*1), '/');
            header('Location: ' . $_SERVER['HTTP_REFERER']);
            exit;
        }
    }
}
