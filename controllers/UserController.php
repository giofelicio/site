<?php
namespace Controllers;

use Exception;
use PDO;
use Twig\Loader\FilesystemLoader;

class UserController{

    private $twig;
    private $PDO;

    public function __construct(){
        $this->twig = new \Twig\Environment(new FilesystemLoader(dirname(__DIR__).'/view'));
        $this->PDO = new PDO(getenv('DB_CONNECTION').':host='.getenv('DB_HOST').';dbname='.getenv('DB_NAME').'', getenv('DB_USER'), getenv('DB_PASSWORD') ,array(PDO::ATTR_PERSISTENT => true));
    }
    public function index(){
        
        return $this->twig->render('/users/users.php', ['url'=> 'http://'.$_SERVER['HTTP_HOST'], 'username' => $_SESSION['username']]);
    }
}
