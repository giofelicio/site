<?php
namespace Controllers;

use Exception;
use PDO;
use Twig\Loader\FilesystemLoader;

class QnController{

    private $twig;
    private $PDO;

    public function __construct(){
        $this->twig = new \Twig\Environment(new FilesystemLoader(dirname(__DIR__).'/view'));
        $this->PDO = new PDO(getenv('DB_CONNECTION').':host='.getenv('DB_HOST').';dbname='.getenv('DB_NAME').'', getenv('DB_USER'), getenv('DB_PASSWORD') ,array(PDO::ATTR_PERSISTENT => true));
    }
    public function index($id){
        $errors = array();
        if(isset($_COOKIE['error_msg'])){
            array_push($errors, $_COOKIE['error_msg']);
            setcookie("error_msg", "", time()-3600);
        }
        if(isset($_COOKIE['success_msg'])){
            $success = $_COOKIE['success_msg'];
            setcookie("success_msg", "", time()-36000);
        }
        $getModels = $this->PDO->prepare('SELECT m.id, m.model, m.type FROM sbr_qnr_qn_models m');
        try {
            $getModels->execute();
            $models = $getModels->fetchAll(PDO::FETCH_OBJ);
        } catch (\Throwable $th) {
            array_push($errors, 'Error when listing ranking models');
        }
        $modelsRa = array();
        $modelsCq = array();
        foreach($models as $model){
            if ($model->type == 1) {
                array_push($modelsRa, $model);
            }
            elseif($model->type == 3){
                array_push($modelsCq, $model);
            }
        }
        $selectModels = 'SELECT m.id FROM sbr_qnr_qn_models m';
        $getQuestions = $this->PDO->prepare('SELECT *,
                                                CASE 
                                                    WHEN q.model IN('.$selectModels.') THEN (SELECT m.model FROM sbr_qnr_qn_models m WHERE id = q.model)
                                                    ELSE q.model
                                                END AS final_model,
                                                case 
                                                    when q.model in('.$selectModels.') then (select m.value from sbr_qnr_qn_models m where id = q.model)
                                                    else q.value
                                                end as final_value
                                            FROM sbr_qnr_qn q WHERE id_sbr_qnr = :id');
        try {
            $getQuestions->execute([':id' => $id]);
            $questions = $getQuestions->fetchAll(PDO::FETCH_OBJ);
        } catch (\Throwable $th) {
            array_push($errors, 'Error when listing ranking models');
        }
        foreach($questions as $key =>  $qn){
            $qn->created_at = date('d-m-Y H:i:s',strtotime($qn->created_at));
            if(!is_numeric($qn->final_model) && (!is_numeric($qn->final_value) || $qn->value == null)){
                $modelQn = explode('#', $qn->final_model);
                $value = explode('#', $qn->final_value);
                if(count($modelQn) == count($value)){
                    $qn->model = $modelQn;
                    $qn->value = $value;
                }
            }
            else{
                
            }
        }
        return $this->twig->render('/qn/qn.php', ['url'=> 'http://'.$_SERVER['HTTP_HOST'], 
                                                    'username' => $_SESSION['username'], 
                                                    'modelsRa' => $modelsRa,
                                                    'modelsCq' => $modelsCq,
                                                    'reference' => $id,
                                                    'questions' => $questions,
                                                    'errors' => $errors, 
                                                    'success' => $success]);
    }
    public function create(){
        if(!isset($_POST['reference']) || !isset($_POST['question']) || !isset($_POST['type'])){
            return 0;
        }
        if($_POST['reference'] == '' || $_POST['question'] == '' || $_POST['type'] == ''){
            return 0;
        }
        if($this->findQuestion($_POST['question']) == true){
            return 0;
        }
        if(!is_numeric($_POST['type']) || !is_numeric($_POST['reference'])){
            return 0;
        }
        if($_POST['type'] == 2){
           if($_POST['model'] == 0){
                if(!isset($_POST['anotherModel']) || !isset($_POST['value']) || $_POST['anotherModel'] == '' || $_POST['value'] == ''){
                    return 0;
                }
                elseif(count($_POST['anotherModel']) != count($_POST['value'])){
                    return 0;
                }
                try {
                    $anotherModel = implode('#', $_POST['anotherModel']);
                    $value = implode('#', $_POST['value']);
                } catch (\Throwable $th) {
                    //throw $th;
                }
            }
            
            if(isset($_POST['checkToSave'])){
                if($this->findModel($_POST['type'], $anotherModel) == false){
                    $insertNewModel = $this->PDO->prepare('INSERT INTO site.sbr_group_qn_models(type, model, value) VALUES(:type, :model, :value)');
                    try {
                        $insertNewModel->execute([':type' => $_POST['type'], ':model' => $anotherModel, ':value' => $value]);
                    } catch (Exception $e) {
                        return 0;
                    }
                }
                $check = $_POST['checkToSave'];
            }
            else{
                $check = 0;
            } 
        }
        $dados = array('id_sbr_group' => $_POST['reference'], 
                        'question' => $_POST['question'], 
                        'type' => $_POST['type'], 
                        'model' => ($_POST['type'] == 2)? (($check == 1) ? $anotherModel : $_POST['model']): null,
                        'value' => ($_POST['type'] == 2)? (($check == 1) ? $value : null): null);
        
        $insertQn = $this->PDO->prepare('INSERT INTO site.sbr_group_qn(id_sbr_group, question, type, model, value) VALUES(:id_sbr_group, :question, :type, :model, :value)');
        try {
            $result = $insertQn->execute([':id_sbr_group' => $dados['id_sbr_group'], 
                                            ':question' => $dados['question'], 
                                            ':type' => $dados['type'], 
                                            ':model' => $dados['model'], 
                                            ':value' => $dados['value']]);
            if($result){
                return 1;
            }
            else{
                return 0;
            }
        } catch (Exception $e) {
            return 0;
        }
    }
    private function findModel($type, $model){
        $select = $this->PDO->prepare('SELECT * FROM site.sbr_group_qn_models WHERE type = :type AND model = :model;');
        try {
            $select->execute([':type' => $type, ':model' => $model]);
            $models = $select->fetch(PDO::FETCH_OBJ);
            if($models){
                return true;
            }
        } catch (\Throwable $th) {
            return true;
        }
        return false;
    }
    private function findQuestion($question){
        $select = $this->PDO->prepare('SELECT * FROM site.sbr_group_qn WHERE question = :question');
        try {
            $select->execute([':question' => $question]);
            $question = $select->fetch(PDO::FETCH_OBJ);
            if($question){
                return true;
            }
            else{
                return false;
            }
        } catch (\Throwable $th) {
            return true;
        }
        return false;
    }

    public function savequestion(){
        if(!isset($_POST['idQuestion']) || !isset($_POST['answer'])){
            echo 0;
        }
        if($_POST['idQuestion'] == '' || $_POST['answer'] == ''){
            echo 0;
        }
        if(!is_numeric($_POST['idQuestion'])){
            echo 0;
        }
        $updateQn = $this->PDO->prepare('UPDATE sbr_qnr_qn SET answer = :answer WHERE id = :id');
        try {
            $result = $updateQn->execute([':answer' => $_POST['answer'], ':id' => $_POST['idQuestion']]);
            if($result){
                echo 1;
            }
            else{
                echo 0;
            }
        } catch (Exception $e) {
            echo 0;
        }
    }
}
