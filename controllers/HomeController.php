<?php

namespace Controllers;

use PDO;
use Twig\Loader\FilesystemLoader;

class HomeController{
    private $twig;
    private $PDO;

    public function __construct(){
        $this->twig = new \Twig\Environment(new FilesystemLoader(dirname(__DIR__).'/view'));
        $this->PDO = new PDO(getenv('DB_CONNECTION').':host='.getenv('DB_HOST').';dbname='.getenv('DB_NAME').'', getenv('DB_USER'), getenv('DB_PASSWORD') ,array(PDO::ATTR_PERSISTENT => true));
    }
    public function index(){
        echo $this->twig->render('/home/home.php', ['url'=> 'https://'.$_SERVER['HTTP_HOST'], 'username' => $_SESSION['username']]);
    }
}
