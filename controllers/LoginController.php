<?php
namespace Controllers;

use Exception;
use PDO;
use Twig\Loader\FilesystemLoader;

class LoginController{

    private $twig;
    private $PDO;

    public function __construct(){
        $this->twig = new \Twig\Environment(new FilesystemLoader(dirname(__DIR__).'/view'));
        $this->PDO = new PDO(getenv('DB_CONNECTION').':host='.getenv('DB_HOST').';dbname='.getenv('DB_NAME').'', getenv('DB_USER'), getenv('DB_PASSWORD') ,array(PDO::ATTR_PERSISTENT => true));
    }
    public function index(){
        $error_msg = $_COOKIE['error_msg'] ?? null;
        setcookie("error_msg", "", time()-3600);
        return $this->twig->render('/auth/login.php', ['url'=> 'https://'.$_SERVER['HTTP_HOST'], 'error_msg' => $error_msg]);
    }
    public function login(){
        date_default_timezone_set('America/Sao_Paulo');
        session_start(); 
        
        $username = isset($_POST["username"]) ? trim($_POST["username"]) : FALSE; 
        $password = isset($_POST["password"]) ? trim($_POST["password"]) : FALSE; 
        var_dump($password);
        //$data = date('D, d M Y H:i:s'); 
        //date('D, d M Y H:i:s', strtotime("+1 minute",strtotime($data)));
        
        if(!$username || !$password){
            setcookie("error_msg", 'Invalid Username or Passowrd' , mktime()+(60*1), '/');
            header("location: /login");
            exit; 
        }
        $sql = "SELECT * FROM sbr_users WHERE username = :username AND password = :password";
        var_dump($sql);
        $find = $this->PDO->prepare($sql);
        $find->bindValue(':username', $username);
        $find->bindValue(':password', $password);
        try{
            $find->execute();
            $result = $find->fetch(PDO::FETCH_OBJ);
        }
        catch(Exception $e){
            setcookie("error_msg", 'User not found2' , mktime()+(60*1), '/');
            header('location: /login');
        }
        mt_srand (time());
        $token = mt_rand(1000000,999999999);
        $sqlUp = "UPDATE sbr_users SET token = :token WHERE id = :id";
        
        $update = $this->PDO->prepare($sqlUp);
        $update->bindValue(':token', $token);
        $update->bindValue(':id', $result->id);
        try {
             $upResu = $update->execute();
        } catch (Exception $e) {
            header('location: /login');
            exit;
        }
        if($upResu){ 
            $_SESSION["id"]= $result->id;
            $_SESSION["username"] = $result->username;
            $_SESSION["role"]= $result->role;
            $_SESSION["token"]= $token;
            header("Location: /"); 
            exit; 
        }
        else{
            setcookie("error_msg", 'User not found2' , mktime()+(60*1), '/');
            header("Location: /login"); 
            exit; 
        }
    }
    public function logout(){
        session_start(); 
        session_destroy();
        header('location: /login');
        exit;
    }
}
