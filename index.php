<?php

use Buki\Router;

require_once __DIR__.'/bootstrap.php';

$dotenv = Dotenv\Dotenv::createImmutable('./');
$dotenv->load();

$params = [
    'paths' => [
		'controllers' => 'controllers/',
		'middlewares' => 'middlewares/'
    ],
    'namespaces' => [
		'controllers' => 'Controllers\\',
		'middlewares' => 'Middlewares\\'
    ],
    'base_folder' => './',
    'main_method' => 'index'
];

//echo 'teste';
$router = new Router($params);
var_dump($router);
/*
$router->get('/', ['before' => 'AuthMiddleware'], 'HomeController@index');
$router->get('/login', ['before' => 'RedirectAuthMiddleware'], 'LoginController@index');
$router->post('/login', 'LoginController@login');
$router->get('/logout', 'LoginController@logout');

$router->get('/users', ['before' => 'AuthMiddleware'], 'UserController@index');

$router->get('/questionnaries', ['before' => 'AuthMiddleware'], 'QnrController@index');
$router->post('/questionnaries', ['before' => 'AuthMiddleware'], 'QnrController@create');

$router->get('/questionnarie/:id', ['before' => 'AuthMiddleware'], 'QnController@index');
$router->get('/qnrupdate/:id', ['before' => 'AuthMiddleware'], 'QnrController@qnrupdate');


$router->post('/savequestion', ['before' => 'AuthMiddleware'], 'QnController@savequestion');

$router->get('/getdataqnr/:id', ['before' => 'AuthMiddleware'], 'QnrDatatable@getDataQnr');

$router->get('/groups', ['before' => 'AuthMiddleware'], 'GroupsController@index');
$router->post('/groups', ['before' => 'AuthMiddleware'], 'GroupsController@create');
$router->get('/groups/sub/questions/:id', ['before' => 'AuthMiddleware'], 'GroupsController@questions');
$router->post('/groups/sub/question', ['before' => 'AuthMiddleware'], 'QnController@create');
$router->get('/getdatagroups', ['before' => 'AuthMiddleware'], 'GroupDatatable@getDataGroups');
$router->get('/getdataqn/:id', ['before' => 'AuthMiddleware'], 'QnDatatable@getDataQn');

$router->get('/groups/sub/:id', ['before' => 'AuthMiddleware'], 'GroupsController@subgroup');
$router->post('/groups/sub', ['before' => 'AuthMiddleware'], 'GroupsController@createsub');
$router->get('/groups/sub/getdata/:id', ['before' => 'AuthMiddleware'], 'GroupDatatable@getdatasubgroups');

$router->run();*/