create database site;
create table users(
	id int(11) auto_increment primary key,
    username varchar(30),
    password varchar(30),
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);
insert into users(username, password) values('giovannifc', '09112013');
SELECT * FROM users WHERE username = 'giovannifc' AND password = '09112013';
alter table users add token varchar(60)  after password;
alter table users add level tinyint(2)  after token;
alter table users modify column level tinyint(2) default 0;
describe users;
select * from users;
drop trigger users_AFTER_UPDATE;

delimiter //
CREATE TRIGGER `site`.`sbr_qnr_AFTER_INSERT` AFTER INSERT ON `sbr_qnr` FOR EACH ROW
BEGIN
	INSERT INTO sbr_auth_qnr_users(id_sbr_qnr, id_sbr_users) VALUES(NEW.id, NEW.create_by);
END//
delimiter ;

INSERT INTO `site`.`sbr_users` (`id`, `name`, `username`, `email`, `password`, `role`) VALUES ('1', 'Giovanni Felicio', 'giovannifc', 'giovanni@smartbr.com', '09112013', '5');
INSERT INTO `site`.`sbr_qnr`(id_sbr_sec, create_by, name) VALUES(1, 1, 'Evaulation');
